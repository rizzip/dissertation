%%
%% GMU LaTeX PhD Dissertation Format Template
%%
%% Developed by:
%%      Daniel O. Awduche and Christopher A. St. Jean
%%      Communications and Networking Lab
%%      Dept. of Electrical and Computer Engineering
%%
%% Notes on usage can be found in the accompanying USAGE_NOTES.txt file.
%%
%%**********************************************************************
%% Legal Notice:
%% This code is offered as-is without any warranty either
%% expressed or implied; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE!
%% User assumes all risk.
%% In no event shall any contributor to this code be liable for any damages
%% or losses, including, but not limited to, incidental, consequential, or
%% any other damages, resulting from the use or misuse of any information
%% contained here.
%%**********************************************************************
%%
%% $Id: GMU_dissertation_template.tex,v 1.7 2007/05/02 02:20:38 Owner Exp $
%%

\documentclass[11 pt]{report}

%%  The file ``gmudissertation.sty''  is the GMU latex style file and
%%   should be placed in the same directory as your LaTeX files
\usepackage{gmudissertation}

%%
%% other packages that need to be loaded
%%
\usepackage{graphicx}                    %   for imported graphics
\usepackage{amsmath}                     %%
\usepackage{amsfonts}                    %%  for AMS mathematics
\usepackage{amssymb}                     %%
\usepackage{amsthm}                      %%
\usepackage[normalem]{ulem}              %   a nice standard underline package
\usepackage[noadjust,verbose,sort]{cite} %   arranges reference citations neatly
\usepackage{setspace}                    %   for line spacing commands
\usepackage{color}
\usepackage{cleveref}

\usepackage{thmtools}
\usepackage{enumitem}

\usepackage{caption}
\usepackage{subcaption}

%% The file ``mydissertationabbrev.sty'' is an (optional) personalized file that
%% may contain any and all LaTeX command (re)definitions that will be used
%% throughout the document
%\usepackage{mydissertationabbrev}

\usepackage{notation/general}
\usepackage{notation/sdecap}
\usepackage{notation/syscap}

%
\crefname{problem}{problem}{problems}
\Crefname{problem}{Problem}{Problems}
%
\crefname{hypothesis}{hypothesis}{hypotheses}
\Crefname{hypothesis}{Hypothesis}{Hypotheses}

\beforedoc

\begin{document}
\initgeneralnotation
\newtheorem{definition}{Definition}[section]
\newtheorem{theorem}[definition]{Theorem}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollary}[definition]{Corollary}
\newtheorem{lemma}[definition]{Lemma}
\newtheorem{remark}[definition]{Remark}
\newtheorem{conjecture}[definition]{Conjecture}
\newtheorem{problem}[definition]{Problem}
\newtheorem{example}[definition]{Example}

%% In this section, all of the user-specific fields to be used in the
%% title pages are set
\title{On Validated Equilibria and Bifurcations\\
        in Materials Science and Stochastic Dynamics}
\onelinetitle{On Validated Equilibria and Bifurcations in Materials Science and Stochastic Dynamics}
\author{Peter Rizzi}
\degree{Doctor of Philosophy}
\doctype{Dissertation}
\dept{Mathematical Sciences}
\discipline{Mathematics}

% \seconddeg{Master of Science}
% \seconddegschool{My Former School}
% \seconddegyear{Year of second degree}

\firstdeg{Bachelor of Science}
\firstdegschool{George Mason University}
\firstdegyear{2015}

\degreeyear{2022}

% Note: semester name should be written in its full-form. For example, Fall Semester, not just Fall.
\degreesemester{Spring Semester}

\advisor{Dr. Thomas Wanner}

\firstmember{Dr. Evelyn Sander}

\secondmember{Dr. Tim Sauer}

\thirdmember{Dr. Juan-Raul Cebral}

\depthead{Dr. Maria Emelianenko}

\assocDean{Dr. Donna M. Fox}

% The current dean is Lloyd J. Griffiths
\deanITE{Dr. Fernando R. Miralles-Wilhelm}

%%
%% Introductory pages
%%

% Note: The signature sheet is set according to the requirements of the Volgenau School of
% Information Technology and Engineering. If your college/school requirement is different,
% please make appropriate changes in the "signaturepage" section of gmudissertation.sty file.
\signaturepage

\titlepage

% copyright technically optional but should be included in to avoid potential pagination problems
\copyrightpage

%%
%% Dedication page
%%

\dedicationpage

\noindent I dedicate this dissertation to my past, current, and future family.

%%
%% Acknowledgements
%%

\acknowledgementspage

\noindent
I first thank my advisor, Dr. Thomas Wanner, for welcoming my interests outside of prescribed coursework,
for his patient and passionate instruction, and for key insights that helped sculpt this dissertation.
Second, I would like to thank Dr. Tim Sauer for encouraging me to enroll in graduate school and providing
personalized coursework, dedication, and research assistantships that helped broaden my mathematical education.
Third, each of my professors at George Mason University deserve a resounding thanks for their dedication and high standards.
Fourth, I would like to thank Brenda Friess, Pam Maloney, Phil Wilks, and the rest of my co-workers at CACI
for their flexibility, understanding, and encouragement.
Finally, I would like to thank my wife, Emma Rizzi, for her unyielding support and honesty in the face of
the sacrifices of my academic pursuit and our dog Suzy for her consistent requests for neck rubs and walks.

%%
%% Table of contents, list of tables, and lists of figures
%%

\tableofcontents

\listoftables

\listoffigures

%%
%% Abstract
%%
\abstractpage

% The first page of the abstract
Partial Differential Equations (PDEs) and their solutions provide a theoretically rich,
widely applicable, and analytically challenging subfield of mathematics and physics.
In the applied setting, analytical solutions are rarely known, and one must resort to numerical methods.
These methods are, at their best, approximations to the exact solution of the PDE
and quickly exceed the computational ability of humans, making a computer
necessary to execute them efficiently.
While many of these methods,
such as the Finite Element Method (FEM) and the Spectral Method,
produce good quality approximations, problems exist where the solution to the numerical formulation
is in fact not close to a solution of the PDE.
The proliferation of computing in scientific applications demands exploration of verification methods for numerical solutions.
We explore two special cases: the Fokker-Planck-Kolmogorov Equation and a variant of the Ohta-Kawasaki model of block copolymers.

The Fokker-Planck-Kolmogorov Equation stems from the study of Stochastic Differential Equations (SDEs) and,
as a Partial Differential Equation (PDE), provides a fundamental link between SDEs and PDEs.
The principal object of study in this case is the transition probability density associated with the stochastic system.
Under suitable conditions, there 

%% Be sure to leave a line of whitespace immediately before this line!!!!!
%% (If this comment segment runs together with the preceding text, you might
%%  see the second page of the abstract numbered "0".)
%%
%% If the abstract is more than one page, then place this line PRECISELY
%% at the page break; otherwise, comment it out.  (See note about this line
%% in the usage notes.)
%%
\abstractmultiplepage
\noindent
exists a distribution that is invariant through time even though the paths of the SDE
move.
We provide theoretical estimates for the application of a constructive version of the Implicit Function Theorem
to verify numerical solutions to the eigenvalue form of the Fokker-Planck-Kolmogorov Equation.
While our numerical examples focus on the invariant distribution (i.e., zero eigenvalue), the theory extends without modification
to the nonzero eigenvalue case.

Block copolymers play an important role in materials sciences and have
found widespread use in many applications. From a mathematical perspective,
they are governed by a nonlinear fourth-order partial differential equation
which is a suitable gradient of the Ohta-Kawasaki energy. While the equilibrium
states associated with this equation are of central importance for the 
description of the dynamics of block copolymers, their mathematical study
remains challenging. We develop computer-assisted 
proof methods which can be used to study equilibrium solutions in block copolymers 
consisting of more than two monomer chains, with a focus on triblock
copolymers. This is achieved by establishing a computer-assisted proof
technique for bounding the norm of the inverses of certain fourth-order
elliptic operators, in combination with an application of a constructive
version of the implicit function theorem. While these results are only
applied to the triblock copolymer case, we demonstrate that the obtained
norm estimates can also be directly used in other contexts such as the
rigorous verification of bifurcation points, or pseudo-arclength
continuation in fourth-order parabolic problems.

%%
%%  the main body of the dissertation
%%
\startofchapters

%% include the chapters one by one (or paste the chapter text in directly if desired)
\include{chapters/introduction}
\initsdecapnotation
\include{chapters/sdecap}
\clearsdecapnotation
\initsyscapnotation
\include{chapters/syscap}
\clearsyscapnotation

%% Note: appendix is now put before bibliography.
%% include the following directives if there are any appendices
\appendix
\appendixeqnumbering

\initsdecapnotation
\include{chapters/sdecap-appendix}
\clearsdecapnotation

%%
%%  bibliography
%%

%% list all of the BibTeX files here for the WinEdt project (if applicable)
%GATHER{bibfile.bib}

%% any bibliography style can be used, but IEEEtran.bst is ideally suited to
%% electrical engineering references

% \bibliographystyle{IEEEtran}
\bibliographystyle{abbrv}
% \bibliography{IEEEfull,bibfile}
\bibliography{ref,invnormbnd-symbif-combined,rizzi}

%%
%% curriculum vitae
%%
\cvpage

\noindent 
% Include your \emph{curriculum vitae} here detailing your background,
% education, and professional experience.
Peter A. Rizzi attended George Mason University, where he received his
Bachelor of Science in Mathematics and Bachelor of Science in Economics in 2015.
He then went to work for CACI International in software development, and he returned
to George Mason in 2018 to pursue his Doctorate in Mathematics, which he completed
in 2022.
\end{document}
