\chapter[Introduction]{Introduction}
\label{chap:intro}

Partial Differential Equations (PDEs) and their solutions provide a theoretically rich,
widely applicable, and analytically challenging subfield of mathematics and physics.
% 
In the applied setting, analytical solutions are rarely known and one must resort to numerical methods.
% 
% The difficulty of finding analytical solutions to applied problems has led to a proliferation of numerical
% solution methods which, at their best, are approximations.
These methods are, at their best, approximations to the base PDE
and quickly exceed the computational ability of humans, making a computer
necessary to execute them efficiently.
While many of these methods,
such as the Finite Element Method (FEM) and the Spectral Method,
produce good quality approximations, problems exist where the solution to the numerical formulation
is in fact not even close to a solution to the base PDE.
The discretized version of Emden's Equation is shown in \cite{rump_verification_2010} to have 
a positive numerical solution that is not symmetric.
This is in direct contradiction with the result of \cite{gidas_symmetry_1979} which proves the true solution
is in fact symmetric.
The difficulty here lies in determining when a numerical solution with small residual is near a true solution, and
demands the development of verification methods to ensure the numerical
approximations are in fact close to true solutions.

This has aided the development of computer-assisted proofs, which have applications as far back
as the Four Color Theorem in 1976.
More recent applications include the Kepler conjecture, the existence of chaos, and the existence of a strange Lorenz attractor \cite{tucker:99}.
While this field encompasses virtually any use of a computer to help with mathematical proof,
we see two variations: constructive proof assistants
(e.g., Coq \cite{coq}, Lean \cite{felty_lean_2015})
and numerical computation.
We are interested in applications to PDEs and the predominant approach in the literature is that
of calculation, thus we focus on verification methods that rely on guaranteed bounds of computation.
Recent work develops such methods through a combination of fixed point arguments
and interval arithmetic; see \cite{rump_verification_2010,sander:wanner:21a,sander:wanner:21a,plum:08a,plum:09a,nakao_numerical_2011,nakao_numerical_2019,lessard:sander:wanner:17a}
and references therein.
While clearly less direct than standard techniques, rigorous numerics shift uncertainty from traditional analysis to hardware and software implementations \cite{plum:08a,plum:09a}. 
This shift allows analysis of (potentially) more difficult problems and can provide explicit rigorous information regarding solutions.
The general process for a computer-assisted proof (CAP) follows \cite{rump_verification_2010}:
\begin{enumerate}
    \item \label{chap:intro:cap-step:1} Find an approximate solution through numerical techniques
    \item \label{chap:intro:cap-step:2} Formulate a mathematical theorem whose assumptions can be verified by computation
    \item \label{chap:intro:cap-step:3} Use interval arithmetic to verify these bounds and show there is an actual solution close to the approximate solution
\end{enumerate}
We emphasize that the first step requires no rigor at all, thus allowing the use of any numerical method that can approximate close enough,
while the second step usually involves rigorous bounds for certain operator norms.

As a staple of computer-assisted proofs, the numerical approximation to a solution is a key ingredient in verification methods.
In principle, so long as one can approximate sufficiently well, any numerical method is acceptable as no rigor is required.
For PDEs, a widely applicable and storied approach is the Finite Element Method. 
This method aims to partition the domain $\Omega$ and focus locally on the pieces to approximate solutions using piece-wise polynomials.
Importantly, partitioning the domain can be done in many ways, affording great flexibility and breadth of applications.
More precisely, one transforms the original PDE into its variational form and enforces the resulting problem for all functions in a suitable finite dimensional approximation space $S_h$ where $h$ is a parameter (e.g., mesh diameter).
An example space $S_h$ is the piece-wise linear Dirichlet space $H_0^1(\Omega)$ spanned by tent functions.
The variational form is then transformed into a finite dimensional system and solved using numerical methods.
Detailed background and theory can be found in \cite{ciarlet_handbook_1990,larson_finite_2013}.
While the Finite Element Method is effective, it is not the only option.
Since this step in the computer-assisted proof process does not require any rigorous arithmetic, any numerical method
can be used so long as the method has suitable convergence.
On the classical side, alternatives include Finite Differences and Spectral/Fourier Expansion as used in \cite{sander:wanner:21a}.
Recent lines of inquiry also explore the use of neural networks and neural operators for PDE solution, although convergence is still an issue \cite{raissi_physics-informed_2019,li_fourier_2021}.
However, these modern techniques present challenges in the theoretical development of bounds for computer-assisted proofs since a choice of basis is important.
Furthermore, many computer-assisted proof problems require not only a solution but also its gradient.
The Finite Element Method is still applicable here, but one could also use Mixed Finite Elements to directly compute the solution and its gradient as opposed to inferring a gradient from the solution.
In the end, one should choose the numerical method best suited for the problem at hand to complete Step~\ref{chap:intro:cap-step:1}.

To complete Step~\ref{chap:intro:cap-step:2}, one usually turns to fixed point problems in Banach spaces or on compact domains.
This affords the use of either completeness (Banach's Fixed Point Theorem) or compactness (Schauder's Fixed Point Theorem)
to deduce existence of a solution. 
In this work, we make use of a constructive version of the implicit function theorem to obtain existence and uniqueness simultaneously
while also maintaining extensibility to bifurcation analysis.
This theorem is presented in~\cite{sander:wanner:16a}, which is based on similar results
in~\cite{plum:09a, wanner:17a}, but we state an applicable version here to remain self-contained.
In order to apply the constructive implicit function theorem, one must establish
the following four assumptions, where $\cX,\cY$ are Banach spaces and $\cF : \R \times \cX \to \cY$ is a smooth operator:
%
\begin{itemize}
%
\item[(H1)] Assume that we have found a numerical approximation
$(\lambda^*,w^*) \in \R \times \cX$ of a solution of the
system $\cF(\lambda, w) = 0$. Then one needs to find an explicit
constant~$\rho > 0$ such that
%
\begin{displaymath}
  \left\| \cF(\lambda^*,w^*) \right\|_\cY \le \rho \; .
\end{displaymath}
%
\item[(H2)] Assume that the Fr\'echet derivative~$D_w\cF(\lambda^*,w^*)
\in \cL(\cX,\cY)$ is invertible, and that its inverse~$D_w\cF(\lambda^*,
w^*)^{-1} : \cY \to \cX$ is bounded and satisfies the estimate
%
\begin{displaymath}
  \left\| D_w\cF(\lambda^*,w^*)^{-1} \right\|_{\cL(\cY,\cX)} \le K
  \; ,
\end{displaymath}
%
for some explicit constant $K > 0$, where~$\| \cdot \|_{\cL(\cY,\cX)}$
denotes the operator norm in~$\cL(\cY,\cX)$.
%
\item[(H3)] There exist constants~$L_1, L_2, \ell_w > 0$ and $\ell_\lambda
\ge 0$ such that for all pairs $(\lambda,w) \in \R \times \cX$ with
$\| w - w^* \|_\cX \le \ell_w$ and $|\lambda - \lambda^*| \le \ell_\lambda$
we have
%
\begin{displaymath}
  \left\| D_w\cF(\lambda,w) -
    D_w\cF(\lambda^*,w^*) \right\|_{\cL(\cX,\cY)} \le
    L_1 \left\| w - w^* \right\|_\cX +
    L_2 \left|\lambda - \lambda^* \right| \; .
\end{displaymath}
%
\item[(H4)] There exist constants~$L_3, L_4 > 0$ such that for all
$\lambda \in \R$ with $|\lambda - \lambda^*| \le \ell_\lambda$ one has
%
\begin{displaymath}
  \left\| D_\lambda \cF(\lambda,w^*) \right\|_{\cY} \le
    L_3 + L_4 \left| \lambda - \lambda^* \right| \; ,
\end{displaymath}
%
where~$\ell_\lambda$ is the constant from~(H3).
%
\end{itemize}
%
The constructive implicit function theorem from~\cite{sander:wanner:16a}
then takes the following form.
%
%
\begin{restatable}[Constructive Implicit Function Theorem]{theorem}{thmnift}
\label{chap:related:nift:thm}
%
%
Let~$\cX$ and~$\cY$ be Banach spaces,
suppose~$\cF : \R \times \cX \to \cY$ is Fr\'echet differentiable, and
assume that the pair~$(\lambda^*,w^*) \in \R \times \cX$ satisfies
hypotheses~(H1) through~(H4). Finally, suppose that
%
\begin{equation} \label{chap:syscap:nift:thm1}
  4 K^2 \rho L_1 < 1
  \qquad\mbox{ and }\qquad
  2 K \rho < \ell_w \; .
\end{equation}
%
Then there exist pairs of constants~$(\delta_\lambda,\delta_w)$ with
$0 \le \delta_\lambda \le \ell_\lambda$ and $0 < \delta_w \le \ell_w$,
as well as
%
\begin{equation} \label{chap:syscap:nift:thm2}
  2 K L_1 \delta_w + 2 K L_2 \delta_\lambda \le 1
  \qquad\mbox{ and }\qquad
  2 K \rho + 2 K L_3 \delta_\lambda + 2 K L_4 \delta_\lambda^2
    \le \delta_w  \; ,
\end{equation}
%
and for each such pair the following holds. For every~$\lambda \in \R$
with $|\lambda - \lambda^*| \le \delta_\lambda$ there exists a uniquely
determined element~$w(\lambda) \in \cX$ with $\| w(\lambda) - w^* \|_\cX
\le \delta_w$ such that $\cF(\lambda, w(\lambda)) = 0$.
In other words, if we define
%
\begin{displaymath}
  \cB_{\delta_w}^\cX = \left\{ w \in \cX \; : \;
    \left\| w - w^* \right\|_\cX \le \delta_w \right\}
  \quad\mbox{ and }\quad
  \cB_{\delta_\lambda}^\R = \left\{ \lambda \in \R \; : \;
    \left| \lambda - \lambda^* \right| \le \delta_\lambda \right\}
  \; ,
\end{displaymath}
%
then all solutions of the nonlinear problem $\cF(\lambda,w)=0$ in the
set $\cB_{\delta_\lambda}^\R \times \cB_{\delta_w}^\cX$ lie on the graph
of the function $\lambda \mapsto w(\lambda)$.  In addition, so long as $\cF$
is infinitely differentiable, the function
$\lambda \mapsto w(\lambda)$ is infinitely-many times Fr\'echet
differentiable.
\end{restatable}

One still needs to verify the preconditions to \Cref{chap:related:nift:thm} in a rigorous manner.
Many problems require \emph{floating point} arithmetic where rounding is ubiquitous given the finite capacity of digital computers.
An extensive overview of rigorous verification methods can be found in \cite{rump_verification_2010},
but the main ingredient for this work is \emph{interval} arithmetic.
Instead of working on floating point numbers, interval arithmetic works on enclosures (intervals) of numbers.
That is, each interval is represented by a pair of floating point numbers, and the resulting arithmetic produces a guaranteed enclosure of the result.
This approach is necessary because of the finite capacity of floating point numbers and the consequential inability to represent certain numbers (e.g., $\pi$, $e$, $0.7$, etc...).
We make use of INTLAB in all verified numerical examples to complete Step~\ref{chap:intro:cap-step:3}.

% {\color{red}The following paragraph echos what was said in \Cref{chap:intro}}

To see the importance of numerical verification, one need only look at Section 1.2 in \cite{rump_verification_2010}. The author presents 3 problems for which discretizations and rounding errors result in incorrect solutions \emph{with small residual}. 
In particular, Emden's equation $-\Delta u = u^2$ is used to show that the discretized equation has a positive numerical solution that is not symmetric and so cannot be a true solution as it contradicts the result of \cite{gidas_symmetry_1979}.
% A second example from \cite{rump_1994} shows that even if higher precision calculations agree, the computed solution may still be far from a true solution. In this example, on e only has to compute the value of an expression
A general solution to such problems is the proper use of interval arithmetic to obtain rigorous enclosures of true solutions.
The use of interval arithmetic, as detailed in \cite{rump_verification_2010} and implemented in INTLAB \cite{rump:99a} produces such enclosures.
Since we aim to study parabolic PDEs like \cref{chap:sde:eq:fokker-planck} below, verification methods in infinite dimensions are of particular interest in this study.

This dissertation fits into a rich literature seeking to develop rigorous validation
methods for dynamical structures, with applications in bifurcation analysis.
% {\color{red}Need some more stuff here?}
% In \Cref{chap:related} we discuss existing verification methods and motivate our contributions, which are twofold.
% First, we develop inverse norm bounds and Lipschitz estimates for
% general uniformly elliptic operators arising from the Fokker-Planck-Kolmogorov equation.
% Second, we develop inverse norm bounds for a particular class of linear operators from various materials science problems.
% In both cases, the intention is to apply the constructive implicit function theorem \cite{sander:wanner:21a}
% and obtain a computer-assisted proof method.
% We include the theorem statement at the end \Cref{chap:related} and emphasize that it forms the backbone of this work.
%
First, we recognize that
Stochastic Differential Equations (SDEs) form a rich subfield of mathematics and physics,
and their deep connection to PDEs yields the Fokker-Planck-Kolmogorov equation involving transition probabilities.
Analytically, this equation is notoriously challenging, especially in more than one spatial dimension.
Thus, one is usually limited to numerical methods and CAPs.
However, the existing CAP literature has predominantly focused on Laplacian operators and this covers
only a small class of Fokker-Planck-Kolmogorov equations.
In \Cref{chap:sde}, we execute our program to develop theoretical estimates for use in 
% the Constructive implicit function Theorem \cite{sander:wanner:21a}
computer-assisted proofs for general uniformly elliptic operators.
% This program is motivated through the lens of Stochastic Differential Equations (SDEs)
% and their associated Fokker-Planck-Kolmogorov Equations.
While we are particularly interested in invariant measures, we develop everything in the context of
elliptic eigenvalue problems.
This generality yields the invariant measures and allows further analysis of measure evolution via the eigenpairs corresponding to nonzero eigenvalues.
Furthermore, our formulations extend naturally to questions regarding bifurcation of invariant measures.
%
% In \Cref{chap:syscap} we examine the materials science problem of block copolymers
% and the application of computer-assisted proofs.
Second,
block copolymers play an important role in materials sciences and have
found widespread use in many applications. From a mathematical perspective,
they are governed by a nonlinear fourth-order partial differential equation
which is a suitable gradient of the Ohta-Kawasaki energy. While the equilibrium
states associated with this equation are of central importance for the 
description of the dynamics of block copolymers, their mathematical study
remains challenging.
In \Cref{chap:syscap}, we develop computer-assisted 
proof methods which can be used to study equilibrium solutions in block copolymers 
consisting of more than two monomer chains, with a focus on triblock
copolymers.
While these results are only
applied to the triblock copolymer case, we demonstrate that the obtained
norm estimates can also be directly used in other contexts such as the
rigorous verification of bifurcation points, or pseudo-arclength
continuation in fourth-order parabolic problems.

In both programs, the backbone of the computer-assistance is a constructive
version of the implicit function theorem to a differential operator.
% We provide a self-contained statement of this theorem, called the constructive implicit function theorem \cite{sander:wanner:16a},
% at the end of \Cref{chap:related}.
% In short, this theorem hinges on the verification of 4 main assumptions:
% \begin{enumerate}
%     \item An approximate solution and rigorous bound on the norm of the residual
%     \item Two Lipschitz type bounds for the linearization
%     \item A bound on the norm of the inverse of the linearization at the approximate solution
% \end{enumerate}
%
For the Fokker-Planck-Kolmogorov equation, we provide estimates for (H1-4).
That is, we develop both Lipschitz estimates and inverse norm bounds
for the general uniformly elliptic operator.
In the case of the materials science operators, the residual norm amounts to evaluating 
a finite sum in interval arithmetic,
and we provide inverse norm bounds for an entire class of linear operators. 
However, the Lipschitz estimates are problem-specific
and thus limited to the example triblock copolymer problem.