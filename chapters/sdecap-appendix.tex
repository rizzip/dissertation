\chapter{Details for \Cref{chap:sde:lem:h2-bound}}
\label{app:sde:lem:h2-bound}

In this chapter, we prove in detail an upper bound for the standard $\HTwo$ seminorm.
In particular, we aim for a bound of the form
\begin{displaymath}
    \sum_{i,j=1}^n u_{x_i x_j}^2 \le C \left\|\A u \right\|_{\LTwo}
.\end{displaymath}
The details are rather involved, but based upon simple pieces.
We first recall that $\A$ is a symmetric and uniformly elliptic second order differential operator
subject to homogeneous Dirichlet boundary conditions on the bounded open domain $\Omega$.
We begin with the following lemma.

\begin{lemma}
	\label{app:sde:lem:h2-bound:lem:norm-Au-ge-u}
	Let $u \in \HZeroOne \cap \HTwo$ and $\eta_1$ denote the minimal point in the spectrum of $\A $.
	Then $\left\|{\A u} \right\|_{\LTwo}\ge \eta_1 \left\| {u} \right\|_{\LTwo}$.
\end{lemma}
\begin{proof}
	Classic PDE theory shows that $\A $ has a compact inverse \cite[Lemma 9.20]{renardy_introduction_2004}. Therefore, the Hilbert-Schmidt theorem implies the existence of a complete orthonormal set of eigenfunctions $\psi_k$ of $\A $ in $\LTwo$. 
	Let $\A \psi_k = -\eta_k \psi_k$ for $k \in \mathbb{N}$. 
    Then $\eta_k > 0$ for all $k \in \mathbb{N}$ and we can assume 
    $0<\eta_1$ and $\eta_k \le \eta_{k+1}$.
	Now consider the decomposition of $u \in \HZeroOne \cap \HTwo$:
	\begin{align*}
		u=\sum_{i=1}^{\infty} \alpha_i\psi_i \in \HZeroTwo \subset \LTwo
	.\end{align*}
	Then $\left\|{u} \right\|_{\LTwo}^{2} = \sum_{i=1}^{\infty} \alpha_i^{2}$ and
	\begin{align*}
		\A u = \sum_{i=1}^{\infty} \alpha_i\A \psi_i = -\sum_{i=1}^{\infty} \alpha_i \eta_i \psi_i
	.\end{align*}
	Therefore,
	\begin{align*}
		\left\| {\A u} \right\|_{\LTwo}^{2} = \sum_{i=1}^{\infty} \alpha_i^{2} \eta_i^{2} \ge \eta_1^{2}\sum_{i=1}^{\infty} \alpha_i^{2} = \eta_1^{2}\left\| {u} \right\|_{\LTwo}^{2}
	.\end{align*}
	Taking the square root of both sides completes the proof.
\end{proof}
\noindent
The next result aims to bound the first derivatives of $u$ by the norm $\left\| {\A u} \right\|_{\LTwo}$.
This can be done similarly to \cite[p. 45]{plum_explicit_1992} as follows.

\begin{lemma}
	\label{app:sde:lem:h2-bound:lem:norm-Au-ge-nu}
	For $u \in \HZeroOne \cap \HTwo$, we have $\left\| {\A u} \right\|_{\LTwo} \ge \nu \eta_1 \left\| {\nabla u} \right\|_{\LTwo}$ 
    with $\eta_1$ as in \Cref{app:sde:lem:h2-bound:lem:norm-Au-ge-u} and $\nu > 0$ the uniform ellipticity constant
	i.e. such that $\xi^T a \xi \ge \nu \| \xi \|_2^2$ for all $\xi \in \R^n$.
\end{lemma}
\begin{proof}
	Let $u \in C_0^{\infty}(\Omega)$. Then
	\begin{align*}
		\operatorname{div}(u \cdot a \nabla u) &= \operatorname{div}\left( u \cdot \sum_{j=1}^{n} a_{ij}u_{x_j}\right)
						       = \sum_{i=1}^{n} \left( u \cdot \sum_{j=1}^{n} a_{ij}u_{x_j} \right)_{x_i}  \\
						       &= \sum_{i,j=1}^{n} \left( u_{x_i} a_{ij} u_{x_j}+u \left( a_{ij}u_{x_j} \right) _{x_i} \right)
						       = \nabla u \cdot a \nabla u + u \operatorname{div}(a\nabla u)
	.\end{align*}
	The divergence theorem, combined with uniform ellipticity, then implies
	\begin{equation*}
		-\int_{\Omega} u \A u \, \mathrm{d} x = -\int_{\Omega} u \operatorname{div}(a\nabla u) \, \mathrm{d} x 
		= \int_{\Omega} \nabla u \cdot a \nabla u \, \mathrm{d} x 
		\ge \nu \int_{\Omega} \left| {\nabla u} \right|^{2} \, \mathrm{d} x
	.\end{equation*}
	Together with \Cref{app:sde:lem:h2-bound:lem:norm-Au-ge-u} and the Cauchy-Schwarz inequality, we obtain
	\begin{align*}
		\left\|{\nabla u} \right\|_{\LTwo}^{2} \le -\frac{1}{\nu}(u,\A u)_{\LTwo}
						   \le \frac{1}{\nu}\left\|{u} \right\|_{\LTwo}\left\|{\A u} \right\|_{\LTwo}
						   \le \frac{1}{\nu \eta_1}\left\|{\A u} \right\|_{\LTwo}^{2}
	.\end{align*}
	The density of $C_0^{\infty}(\Omega)$ in $\HZeroOne \cap \HTwo$ completes the proof.
\end{proof}

We now turn to the involved task of developing the bound for the second derivatives of $u$.
Since we are concerned with the Dirichlet problem, the approach is similar to \cite{ladyzhenskaya_linear_1968}.
In preparation, we emphasize the importance of the uniform ellipticity of $\A $ and the smooth nature of $a(x)$.
The first provides an essential link to a lower bound of the form
\begin{align}\label{app:sde:lem:h2-bound:ineq:unif-ellip-aa}
	\sum_{i,j,k,\ell=1}^{n} \int_{\Omega} a_{ij}a_{k\ell}u_{x_ix_{k}}u_{x_jx_\ell} \, \mathrm{d} x \ge \nu^{2} \sum_{i,j=1}^{n} \int_{\Omega} u_{x_ix_j}^{2} \, \mathrm{d} x
\end{align}
while the second allows us to make use of the compactness of $\overline{\Omega}$ for suitable bounds.
To begin, we first show
\begin{lemma}
	The inequality in \eqref{app:sde:lem:h2-bound:ineq:unif-ellip-aa} holds.
\end{lemma}
\begin{proof}
	Fix an arbitrary $x^{*} \in \Omega$. Then, since $a(x^{*})$ is symmetric, there exists an orthogonal matrix $Q = \left( q_{ij} \right) $ and real numbers $\lambda_1(x^{*}),\lambda_2(x^{*}),\ldots,\lambda_n(x^{*})$ such that
	for each $i=1,\ldots,n$
	\[
		\lambda_i(x^{*}) \ge \nu
	\] 
	and
	\[
		Qa(x^{*})Q^{T} = \operatorname{diag}(\lambda_1(x^{*}),\ldots,\lambda_n(x^{*}))
	.\] 
	This can be rewritten as
	\[
		\sum_{i,j=1}^{n} a_{ij}(x^{*})q_{ki}q_{\ell j} = \lambda_k(x^{*})\delta_{k\ell}
	.\] 
	Now for $x \in \Omega$, set $y = Q(x-x^{*})$. Then we have $x = x^{*}+Q^{T}y$ and we may further define
	\[
		v(y) = u(x) = u(x^{*}+Q^{T}y)
	.\] 
	Since $(Q^{T}y)_{i} = \sum_{j=1}^{n} q_{ji}y_{j}$, we have
	\begin{align*}
		v_{y_k}(y) &= \sum_{i=1}^{n} u_{x_i}(x^{*}+Q^Ty) q_{ki} \\
		v_{y_ky_\ell}(y) &= \sum_{i,j=1}^{n} u_{x_ix_j}(x^{*}+Q^Ty) q_{ki}q_{\ell j}
	.\end{align*}
	This last identity implies
	\[
		\operatorname{Hess}_y(v) = Q \operatorname{Hess}_x(u)Q^{T}
	\]
	which then implies
	\[
	u_{x_kx_\ell} = \sum_{i,j=1}^{n} v_{y_iy_j}q_{ik}q_{j\ell}
	.\] 
	We now obtain
	\begin{align*}
		\sum_{i,j,k,\ell}^{n} a_{ij}(x^{*})a_{k\ell}(x^{*})u_{x_ix_k}u_{x_jx_\ell}
		&= \sum_{i,j,k,\ell=1}^{n} a_{ij}a_{k\ell} \sum_{\alpha,\beta=1}^{n} v_{y_\alpha y_\beta}q_{\alpha i}q_{\beta k}\sum_{\gamma,\zeta=1}^{n} v_{y_{\gamma}y_{\zeta}}q_{\gamma j}q_{\zeta \ell} \\
		&= \sum_{\alpha,\beta,\gamma,\zeta=1}^{n} \left( \sum_{i,j=1}^{n} a_{ij}q_{\alpha i}q_{\gamma j} \right) \left( \sum_{k,\ell=1}^{n} a_{k\ell} q_{\beta k}q_{\zeta\ell} \right) v_{y_\alpha y_\beta}v_{y_\gamma y_\zeta} \\
		&= \sum_{\alpha,\beta,\gamma,\zeta=1}^{n} \left( \lambda_\alpha(x^{*})\delta_{\alpha,\gamma} \right) \left( \lambda_\beta(x^{*})\delta_{\beta,\zeta} \right) v_{y_\alpha y_\beta}v_{y_\gamma y_\zeta} \\
		&= \sum_{\alpha,\beta=1}^{n} \lambda_\alpha(x^{*})\lambda_\beta(x^{*})v_{y_\alpha y_\beta}^{2}
    % \end{align*}
    % \begin{align*}
		\ge \sum_{\alpha,\beta=1}^{n} \nu^{2}v_{y_\alpha y_\beta}^{2}
    .\end{align*}
    The final expression is equal to 
    \begin{align*}
		\nu^{2} \left\| {\operatorname{Hess}_y(v)} \right\|_F^{2}
		&= \nu^{2} \left\| {Q\operatorname{Hess}_x(u)Q^{T}} \right\|_F^{2}
		= \nu^{2} \left\| {\operatorname{Hess}_x(u)} \right\|_F^{2} \\
		&= \nu^{2} \sum_{i,j=1}^{n} u_{x_ix_j}^{2}
	.\end{align*}
    Therefore,
    \begin{equation*}
        \sum_{i,j,k,\ell}^{n} a_{ij}(x^{*})a_{k\ell}(x^{*})u_{x_ix_k}u_{x_jx_\ell} \ge \nu^{2} \sum_{i,j=1}^{n} u_{x_ix_j}^{2}
    \end{equation*}
    as desired.
\end{proof}
\noindent
We now turn our attention to the main result by expanding the differential operator $\A $. The definition implies
\begin{align*}
	\A u &= \operatorname{div}(a\nabla u) = \sum_{i=1}^{n} \left( \sum_{j=1}^{n} a_{ij}u_{x_j} \right)_{x_i}
	= \sum_{i=1}^{n} \sum_{j=1}^{n} \left( a_{ij}u_{x_j} \right)_{x_i}
	 \\&= \sum_{i,j=1}^{n} a_{ij}u_{x_ix_j} + \sum_{i,j=1}^{n} \left( a_{ij} \right) _{x_i}u_{x_j}
.\end{align*}
Squaring this identity yields
\begin{align}
	\begin{split}\label{app:sde:lem:h2-bound:eq:Au-sq}
		\left( \A u \right) ^{2} &= \sum_{i,j,k,\ell=1}^{n} a_{ij}a_{k\ell}u_{x_ix_j}u_{x_kx_\ell}
			       + 2 \sum_{i,j,k,\ell=1}^{n} a_{ij}(a_{k\ell})_{x_k}u_{x_ix_j}u_{x_\ell} + \\
			       & \quad + \sum_{i,j,k,\ell=1}^{n} \left( a_{ij} \right) _{x_i} \left( a_{k\ell} \right)_{x_k}u_{x_j}u_{x_\ell} 
	.\end{split}
\end{align}
Since we are ultimately interested in $\left\| {\A u} \right\|_{\LTwo}$, we integrate over $\Omega$.
In order to apply the uniform ellipticity condition, we must rewrite the integral of the first term in \Cref{app:sde:lem:h2-bound:eq:Au-sq}.
This requires a short detour for an integration by parts formula.
Let $u,v \in C_0^{\infty}(\Omega)$ and define $G_i(x) = \left( 0,\ldots,0,u(x)v(x),0,\ldots,0 \right) $ ($uv$ in the $i$-th coordinate).
Then $\operatorname{div}(G_i(x)) = u_{x_i}v+uv_{x_i}$ and the divergence theorem combined with the homogeneous Dirichlet condition yields:
\begin{align*}
	\int_{\Omega} u_{x_i}v \, \mathrm{d} x = - \int_{\Omega} uv_{x_i} \, \mathrm{d} x
.\end{align*}
We will apply this identity twice. First on the $i$-th coordinate, then on the $\ell$-th coordinate.
The first application yields
\begin{align*}
	\sum_{i,j,k,\ell=1}^{n} \int_{\Omega} a_{ij}a_{k\ell}u_{x_ix_j}u_{x_kx_\ell} \, \mathrm{d} x
	&= -\sum_{i,j,k,\ell=1}^{n} \int_{\Omega} u_{x_j} \left( a_{ij}a_{k\ell}u_{x_kx_\ell} \right) _{x_i} \, \mathrm{d} x \\
	&= -\sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left( a_{ij}a_{k\ell} \right) _{x_i}u_{x_j}u_{x_kx_\ell} \, \mathrm{d} x \\
	& \quad - \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} a_{ij}a_{k\ell}u_{x_j}u_{x_kx_\ell x_i} \, \mathrm{d}x
\end{align*}
and the second application gives
\begin{align*}
	&-\sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left( a_{ij}a_{k\ell} \right) _{x_i}u_{x_j}u_{x_kx_\ell} \, \mathrm{d} x
	+ \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left( a_{ij}a_{k\ell}u_{x_j} \right) _{x_\ell}u_{x_ix_k} \, \mathrm{d} x \\
	&= -\sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left( a_{ij}a_{k\ell} \right) _{x_i}u_{x_j}u_{x_kx_\ell} \, \mathrm{d} x
	+ \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left( a_{ij}a_{k\ell} \right) _{x_\ell}u_{x_j}u_{x_ix_k} \, \mathrm{d} x + \\
	& \quad + \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} a_{ij}a_{k\ell}u_{x_ix_k}u_{x_jx_\ell} \, \mathrm{d} x
.\end{align*}
Thus, we have the following representation of $\displaystyle \sum_{i,j,k,\ell = 1}^n \int_\Omega a_{ij} a_{k\ell} u_{x_i x_k} u_{x_j x_\ell} \, \mathrm{d} x$:
\begin{equation}\label{app:sde:lem:h2-bound:eq:painful-id}
	\begin{aligned}
        % &\sum_{i,j,k,\ell = 1}^n \int_\Omega a_{ij} a_{k\ell} u_{x_i x_k} u_{x_j x_\ell} \, \mathrm{d} x
        % - \sum_{i,j,k,\ell = 1}^n \int_\Omega (a_{ij} a_{k\ell})_{x_i} u_{x_j} u_{x_k x_\ell} \, \mathrm{d} x
        % \\&+ \sum_{i,j,k,\ell = 1}^n \int_\Omega (a_{ij} a_{k\ell})_{x_\ell} u_{x_j} x_{x_i x_k} \, \mathrm{d} x
        % + \sum_{i,j,k,\ell = 1}^n \int_\Omega (a_{ij})_{x_i} (a_{k\ell})_{x_k} u_{x_j} u_{x_\ell} \, \mathrm{d} x
        % \\&+ 2 \sum_{i,j,k,\ell = 1}^n \int_\Omega a_{ij} (a_{k\ell})_{x_k} u_{x_i x_j} u_{x_\ell} \, \mathrm{d} x
		&\int_{\Omega} \left( \A u \right) ^{2} \, \mathrm{d} x
		+ \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left( a_{ij}a_{k\ell} \right) _{x_i}u_{x_j}u_{x_kx_\ell} \, \mathrm{d} x
		\\& \quad - \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left( a_{ij}a_{k\ell} \right) _{x_\ell}u_{x_j}u_{x_ix_k} \, \mathrm{d} x
		- \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} (a_{ij})_{x_i} (a_{k\ell})_{x_k} u_{x_j}u_{x_\ell} \, \mathrm{d} x
        \\& -2 \sum_{i,j,k,\ell = 1}^n \int_\Omega a_{ij} (a_{k\ell})_{x_k} u_{x_i x_j} u_{x_\ell} \, \mathrm{d} x
	\end{aligned}
.\end{equation}
We are finally ready to assemble the main result.
\htwoboundlem*
\begin{proof}
    Applying \Cref{app:sde:lem:h2-bound:ineq:unif-ellip-aa}, the triangle inequality, and the definitions in the statement to \Cref{app:sde:lem:h2-bound:eq:painful-id} yields:
	\begin{align*}
		\nu^{2} \sum_{i,j=1}^{n} u_{x_ix_j}^{2} &\le \left\| {\A u} \right\|_{\LTwo}^{2} + 2M_1 \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left| {u_{x_j}u_{x_kx_\ell}} \right| \, \mathrm{d} x \\
					& \quad + M_2 \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left| u_{x_j}u_{x_\ell} \right|\, \mathrm{d} x + 2M_3 \sum_{i,j,k,\ell=1}^{n} \int_{\Omega} \left| u_{x_ix_j}u_{x_\ell} \right|\, \mathrm{d} x \\
					&= \left\| {\A u} \right\|_{\LTwo}^{2} + 2n(M_1+M_3)\sum_{i,j,k=1}^{n} \int_{\Omega} \left| u_{x_i}u_{x_jx_k} \right|\, \mathrm{d} x 
					\\ & \quad + M_2 n^{2} \sum_{i,j=1}^{n} \int_{\Omega} \left| u_{x_i}u_{x_j} \right|\, \mathrm{d} x
	.\end{align*}
	Next, since $0 \le \left( \sqrt{\epsilon} |a|-\frac{1}{2\sqrt{\epsilon}}|b| \right) ^{2}$ 
	we have $|ab| \le \epsilon a^{2} + \frac{1}{4\epsilon} b^{2}$.
	Thus
	\begin{align*}
		2n(M_1+M_3)\sum_{i,j,k=1}^{n} \int_{\Omega} \left| u_{x_i}u_{x_jx_k} \right|\, \mathrm{d} x
		&\le 2n(M_1+M_3) \sum_{i,j,k=1}^{n} \int_{\Omega} \left( \epsilon u_{x_jx_k}^{2} + \frac{1}{4\epsilon}u_{x_i}^{2} \right)  \, \mathrm{d} x \\
		M_2 n^{2} \sum_{i,j=1}^{n} \int_{\Omega} \left| u_{x_i}u_{x_j} \right|\, \mathrm{d} x
		&\le M_2 n^{2} \sum_{i,j=1}^{n} \int_{\Omega} \left( \frac{1}{2}u_{x_i}^{2}+\frac{1}{2}u_{x_{j}}^{2} \right)  \, \mathrm{d} x
	\end{align*}
	and we obtain the inequality
	\begin{align*}
		\nu^{2}\sum_{i,j=1}^{n} u_{x_ix_j}^{2}&\le \left\| {\A u} \right\|_{\LTwo}^{2}
		+ 2\epsilon n^{2} \left( M_1+M_3 \right) \sum_{i,j=1}^{n} \int_{\Omega} u_{x_ix_j}^{2} \, \mathrm{d} x \\
					      &\quad + \left( \frac{n^{3}}{2\epsilon} \left( M_1+M_3 \right) +M_2 n^{3} \right) \sum_{i=1}^{n} \int_{\Omega} u_{x_i}^{2} \, \mathrm{d} x
	.\end{align*}
	\Cref{app:sde:lem:h2-bound:lem:norm-Au-ge-nu} then produces the simplified inequality
	\begin{align*}
		\nu^{2}\sum_{i,j=1}^{n} u_{x_ix_j}^{2} &\le \left( 1 + \frac{1}{\nu^{2} \eta_1^{2}}\left( \frac{n^{3}}{2\epsilon} \left( M_1+M_3 \right) +M_2 n^{3} \right) \right)  \left\| {\A u} \right\|_{\LTwo}^{2} \\
					      & \quad + 2\epsilon n^{2} \left( M_1+M_3 \right) \sum_{i,j=1}^{n} u_{x_ix_j}^{2}
	.\end{align*}
	This implies that, so long as $2\epsilon n^{2} \left( M_1+M_3 \right) < \nu^{2}$, we have
	\begin{align*}
		\sum_{i,j=1}^{n} u_{x_ix_j}^{2} \le \frac{1}{\nu^{2}-2\epsilon n^{2}(M_1+M_3)}\left( 1 + \frac{1}{\nu^{2} \eta_1^{2}}\left( \frac{n^{3}}{2\epsilon} \left( M_1+M_3 \right) +M_2 n^{3} \right) \right)  \left\| {\A u} \right\|_{\LTwo}^{2}
	.\end{align*}
	Choosing $\epsilon$ so that $2\epsilon n^{2}(M_1+M_3)=\frac{1}{2}\nu^{2}$ completes the proof.
\end{proof}