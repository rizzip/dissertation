function [Ctilde] = computeCtilde(xs,a,dflag,verbose)
    total_iter = 2*length(xs)-5;
    iter_count = 0;

    diagonal_values = zeros(1, length(xs)-2);
    off_diagonals = zeros(1, length(xs)-3);
    a_primitive = polyint(a);
    
    if ~dflag
        diagonal_values = intval(diagonal_values);
        off_diagonals = intval(off_diagonals);
        a_primitive = polynom(polyint(intval(a)));
    end
    
    for k=1:size(diagonal_values, 2)
        iter_count = iter_count + 1;
        if verbose
            print_status(iter_count, total_iter, 10, 'computeCtilde');
        end
        
        if isa(diagonal_values, 'intval')
            diagonal_values(k) = ...
                (xs(k+1)-xs(k))^(-2)*(a_primitive{xs(k+1)}-a_primitive{xs(k)})...
                + (xs(k+2)-xs(k+1))^(-2)*(a_primitive{xs(k+2)}-a_primitive{xs(k+1)});
        else
            diagonal_values(k) = ...
                (xs(k+1)-xs(k))^(-2)*(diff(polyval(a_primitive, [xs(k) xs(k+1)])))...
                + (xs(k+2)-xs(k+1))^(-2)*(diff(polyval(a_primitive, [xs(k+1) xs(k+2)])));
        end
    end
    
    for k=1:size(off_diagonals, 2)
        iter_count = iter_count + 1;
        if verbose
            print_status(iter_count, total_iter, 10, 'computeCtilde');
        end
        
        if isa(off_diagonals, 'intval')
            off_diagonals(k) = -(xs(k+2)-xs(k+1))^(-2)*(a_primitive{xs(k+2)}-a_primitive{xs(k+1)});
        else
            off_diagonals(k) = -(xs(k+2)-xs(k+1))^(-2)*(diff(polyval(a_primitive, [xs(k+1) xs(k+2)])));
        end
    end
    
    i = [
        1:size(diagonal_values, 2),...
        1:size(off_diagonals, 2),... 
        2:size(off_diagonals, 2)+1,...
        length(xs)-1,...
        ];
    j = [
        1:size(diagonal_values, 2),...
        2:size(off_diagonals, 2)+1,...
        1:size(off_diagonals, 2),...
        length(xs)-1,...
        ];
    v = [diagonal_values off_diagonals off_diagonals 1];
    
    Ctilde = sparse(i,j,v,length(xs)-1,length(xs)-1);
end
