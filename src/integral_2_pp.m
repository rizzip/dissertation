function [norm,parts] = integral_2_pp(a,b,lambda,xs,u,dflag,verbose)
%INTEGRAL_2_PP Computes the second integral for the residual bound
%   Constructs polynomial integrand on each mesh element for integration
%
%INPUTS:
% a: SDE generator
% b: SDE coeffs
% lambda: approximate eigenvalue
% xs: interval mesh
% u: approximate eigenfunction (normalized)
% dflag: use 0
% verbose: print progress to console (0 = No, 1 = Yes)

N = prod(size(xs))-1;

norm_sq = 0;
parts = zeros(1,N);

if dflag
    error('not supported')
else
    a = polynom(a);
    b = polynom(b);
    xs = intval(xs);
    u = intval(u);
    lambda = intval(lambda);
    norm_sq = intval(norm_sq);
    parts = intval(parts);
end

u_x = (u(2:end)-u(1:end-1))./(xs(2:end)-xs(1:end-1));

u_x = [u_x u_x(end)];

% for each interval [x_i, x_{i+1}]
for i=1:N
    if verbose
        print_status(i,N,10,'integral_2_pp')
    end
    interval = xs(i:i+1);
    h = interval(2) - interval(1);
    
    % build rho ~ u_x as polynomial
    m_rho = (u_x(i+1)-u_x(i))/h;
    
    rho = polynom([m_rho u_x(i)-m_rho*xs(i)]);
    
    a_rho = rho*a;
    div_a_rho = a_rho';
    
    % build u as polynomial
    u_poly = polynom([u_x(i) u(i)-u_x(i)*xs(i)]);
    
    b_u_poly = b*u_poly;
    div_b_u = b_u_poly';
    
    integrand = (div_a_rho + div_b_u - lambda*u_poly)^2;
    
    primitive = polynom(polyint(intval(integrand.c)));
    
    part = primitive{interval(2)} - primitive{interval(1)};
    parts(i) = part;
    norm_sq = norm_sq + part;
end

norm = sqrt(norm_sq);

end

