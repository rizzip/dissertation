function tests = test_mass_assembler()
tests = functiontests(localfunctions);
end

function test_is_same(testCase)
    x = linspace(-1,1,10);
    n = length(x)-1; % number of subintervals
    M = zeros(n+1,n+1); % allocate mass matrix
    for i = 1:n % loop over subintervals
        h = x(i+1) - x(i); % interval length
        M(i,i) = M(i,i) + h/3; % add h/3 to M(i,i)
        M(i,i+1) = M(i,i+1) + h/6;
        M(i+1,i) = M(i+1,i) + h/6;
        M(i+1,i+1) = M(i+1,i+1) + h/3;
    end
    
    M = M(2:end-1,2:end-1);
    M2 = fem.oned.mass_assembler(x);
    
    assert(max(max(abs(M-M2))) < 1e-15);
end

