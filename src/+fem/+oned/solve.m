function [P,D] = solve(V,G,m,xs)
% inputs are:
%   V, G coefficient POLYNOMIALS for the SDE dX = V dt + G dW
%   m: number of smallest eigenvalues to return
%   xs: initial discretization of domain

    a = 1/2*conv(G,G);

    % change interval/discretizations
    % discretizations can give non symmetic matrices 
    % (e.g. complex eigenvalues, maybe with large real part)
    % non uniform grids
    % xs = linspace(-L,L,10000);

    import fem.oned.*
    
    M = stiffness2(a,V,xs);
    B = mass_assembler(xs);
    
%     [P,D]=eigs(M,B,m,'sm');
    [P,D]=eigs(M,B,m,0);
end