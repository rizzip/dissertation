function M = stiffness( a, V, xs )
%STIFFNESS Constructs the 1-D stiffness matrix for dx = V(x) dt + G(x) dW
%   a is the generator of the stochastic process in polynomial form
%   V is the deterministic portion of the SDE in polynomial form
%   xs is the domain discretization

    % derivative of a
    ax = polyder(a);
    % p = V - ax
    p = polysum(V,-ax);
    pint = polyint(p);
    
    % pxa = p*x - a
    pxa = polysum(conv(p,[1 0]),-a);
    pxaint = polyint(pxa);
    h = diff(xs);
     
    N = length(xs);
    pxaints = zeros(1,N-1);
    pints = zeros(1,N-1);
    for i=1:N-1
        % i-th entry is integral from x(i) to x(i+1)
        pxaints(i) = diff(polyval(pxaint,[xs(i) xs(i+1)]));
        pints(i) = diff(polyval(pint,[xs(i) xs(i+1)]));
    end
    B = zeros(N-2,3);
    for i=1:N-2
        % diag
        B(i,1) = [1/h(i)^2 1/h(i+1)^2]*([1 -xs(i);1 -xs(i+2)].*[pxaints(i) pints(i); pxaints(i+1) pints(i+1)])*ones(2,1);
        % subdiag
        B(i,2) = -1/h(i+1)^2 * ([1 -xs(i+2)].*[pxaints(i+1) pints(i+1)]) * ones(2,1);
        % superdiag
        B(i,3) = -1/h(i+1)^2 * ([1 -xs(i+1)].*[pxaints(i+1) pints(i+1)]) * ones(2,1);
    end
    % shift superdiagonal
    B(:,3) = [0; B(1:N-3,3)];
    M = spdiags(B,[0 -1 1],N-2,N-2);
end

