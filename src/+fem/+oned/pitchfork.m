function [P,D] = pitchfork(c1,c2,s1,s2,m,xs)
% inputs are:
%   c1,c2,s1,s2: coefficient values for the SDE dX = (c1*x-c2*x^3)dt + (s1*x+s2)dW
%   m: number of smallest eigenvalues to return
%   xs: initial discretization of domain

    % a = [1/2*s1^2 s1*s2 1/2*s2^2];
    G = [s1 s2];
    V = [-c2 0 c1 0];

    % change interval/discretizations
    % discretizations can give non symmetic matrices 
    % (e.g. complex eigenvalues, maybe with large real part)
    % non uniform grids
    % xs = linspace(-L,L,10000);

    % M = stiffness(a,V,xs);
    % B = MassAssembler1D(xs);
    
    % [P,D]=eigs(M,B,m,'sm');
    [P,D] = fem.oned.solve(V,G,m,xs);
end