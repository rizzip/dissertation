function M = mass_assembler(xs)
n = length(xs)-1; % number of subintervals
% MM = zeros(n+1,n+1); % allocate mass matrix
MM = zeros(n+1,3);
for i = 1:n % loop over subintervals
    h = xs(i+1) - xs(i); % interval length
    MM(i,1) = MM(i,1) + h/3;
    MM(i+1,1) = MM(i+1,1) + h/3;
    MM(i,2) = MM(i,2) + h/6;
    MM(i,3) = MM(i,3) + h/6;
end
% hs = diff(xs);
% MM(2:(end-1),1) = (hs(1:end-1)+hs(2:end))./3;
% MM(1,1) = hs(1)/3;
% MM(end,1) = hs(end)/3;

% MM(:,2) = [hs./6 0]';
% MM(:,3) = [0;MM(1:,2)];

MM(:,3) = [0;MM(1:n,3)];
M = spdiags(MM(2:end,:),[0,-1,1],n-1,n-1); % should be length(xs)-2 b.c. of Dirichlet B.C.
end

