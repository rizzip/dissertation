function [out] = dint(a,b,p)
%DINT compute \int_a^b p dx for polynomial p
%   Detailed explanation goes here
p_int = polyint(p);
out = diff(polyval(p_int,[a b]));
end

