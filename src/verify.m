function [KN,K,rho_1,rho_2,L1,rho,delta_u,err] = verify(xs,lambda,u,w,a,b,kndflag,knstrategy,verbose)
%VERIFY Verify the solution (lambda, u)
%   xs: mesh used for solution
%   lambda: approximate eigenvalue
%   u: approximate eigenfunction (normalized)
%   w: normalization factor
%   a: generator of the SDE as POLYNOMIAL coefficients
%   b: POLYNOMIAL coefficients of the function b = div(a) - f
%   kndflag: 0 = do KN computation in intvals, 1 = do KN computation in doubles
%   knstrategy: 'neumann' = use neumann result, 'direct' = norm(M,2), 'best' = do both and take narrowest interval
%   verbose: 0 = silent, 1 = print status and other information

err = 0;

ixs = intval(xs);
ilambda = intval(lambda);
iu = intval(u);
iw = intval(w);

a_p = polynom(a);
a_pp = a_p';
assert(all(inf(a_p{ixs}) > 0));

b_p = polynom(b);
b_p_x = b_p';

C_p = (ixs(end)-ixs(1))/2/intval('pi');
C_N = sqrt(1/min(a_p{ixs}.inf));

% start with Lipschitz bound and residual bound
L1 = C_p^2*C_N;

i1 = integral_1_pp(a,xs,u,0,verbose);
i2 = integral_2_pp(a,b,lambda,xs,u,0,verbose);

rho = C_N*i1+C_p*C_N*i2;

% compute K_N
if kndflag
    knw = w;
else
    knw = iw;
end

KN = computeKN(xs,lambda,u,a,b,knw,kndflag,knstrategy,verbose);
if kndflag
    KN = intval(KN);
end
rho_2 = max(KN, 1);

% compute rho_1
b_p_norminf = intval(max(b_p{ixs}.sup));
b_p_x_norminf = intval(max(b_p_x{ixs}.sup));

boldalpha = [pwlnorml2(ixs,iu) b_p_x_norminf*C_p*C_N+b_p_norminf*C_N+abs(ilambda)*C_p*C_N];

% need to be careful here, must satisfy (2.19) and (2.20)
% must leverage Lemma 2.3.8 and take maximum
% C0_h = intval('0.493')*max(ixs(2:end)-ixs(1:end-1));
C0_h = max(ixs(2:end)-ixs(1:end-1))/intval('pi');

n = 1;
nu = min(a_p{ixs}.inf);
M1 = max(sup(a_p{ixs}.*a_pp{ixs}));
M2 = max(sup(a_pp{ixs}.^2));
M3 = max(sup(a_p{ixs}.*a_pp{ixs}));
C = nu^(-2)*sqrt(intval('2'))*sqrt(nu^2 + 2*n^5*(M1+M3) + M2*n^3*nu^2);

C_h = max(C0_h*C_N, C0_h*C);

boldbeta_tmp = b_p_norminf*C_N + C_h*(b_p_x_norminf + abs(ilambda));

boldbeta = KN*sqrt( ...
                pwlnorml2(ixs,iw.*intval(ones(size(xs)))).^2 ...
                +(C_N*b_p_norminf+C_p*C_N*abs(ilambda)).^2 ...
            )...
         + boldbeta_tmp;

gamma = [...
    C_h*norm(boldalpha,2)...
    C_h*boldbeta...
];

rho_1 = norm(gamma, 2);

if rho_1.sup > 1
    warning on
    warning('rho1.sup = %0.6f > 1 so we cannot validate. Try a finer mesh', rho_1.sup);
    err = 1;
end

% compute K
K = rho_2/(1-rho_1);

cift_1 = sup(4*K^2*rho*L1);
if cift_1 > 1
    warning on
    warning('sup(4*K^2*rho*L1) = %0.6f > 1 is too big', cift_1)
    err = 2;
end
cift_2 = sup(2*K*rho);
if cift_2 > 1 % this is \ell_w for SDEs
    warning on
    warning('sup(2*K*rho) = %0.6f > 1 is too big', cift_2)
    err = 3;
end
delta_u = inf(min(1/(2*K*L1), 2*K*rho));

end

