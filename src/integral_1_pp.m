function [norm] = integral_1_pp(a,xs,u,dflag,verbose)
%INTEGRAL_1_PP Computes the first integral for the residual bound
%   Constructs polynomial integrand on each mesh element for integration
%
%INPUTS:
% a: SDE generator
% xs: interval mesh
% u: approximate eigenfunction (normalized)
% dflag: use 0
% verbose: print progress to console (0 = No, 1 = Yes)

norm_sq = 0;

if dflag
    error('not supported')
else
    a = polynom(a);
    xs = intval(xs);
    u = intval(u);
    norm_sq = intval(norm_sq);
end

u_x = (u(2:end)-u(1:end-1))./(xs(2:end)-xs(1:end-1));

u_x = [u_x u_x(end)];

% for each interval [x_i, x_{i+1}]
N = prod(size(xs))-1;
for i=1:N
    if verbose
        print_status(i,N,10,'integra1_1_pp')
    end
    
    interval = xs(i:i+1);
    h = interval(2) - interval(1);
    
    % build rho ~ u_x as polynomial
    m_rho = (u_x(i+1)-u_x(i))/h;
    
    rho = polynom([m_rho u_x(i)-m_rho*xs(i)]);
    
    rho_m_u_x = rho - polynom(u_x(i));
    a_rho_m_u_x = a*rho_m_u_x;
    
    integrand = (a_rho_m_u_x)^2;
    
    primitive = polynom(polyint(intval(integrand.c)));
    
    part = primitive{interval(2)} - primitive{interval(1)};
    
    norm_sq = norm_sq + part;
end

norm = sqrt(norm_sq);

end

