function [norm] = pwlnorml2(xs,v)
%PWLNORML2 Computes the L2-norm of a piecewise linear function
%   xs: mesh in size 1 x N or N x 1
%   v: function valus on mesh

if isa(v,'intval')
    norm_sq = intval(0);
    
    xs = intval(xs);
    
    for i=1:length(xs)-1
        dx = xs(i+1) - xs(i);
        dv = v(i+1) - v(i);
        dvdx = dv/dx;

        p = polynom([dvdx v(i)-dvdx*xs(i)]);
        pp = p*p;
        
        primitive = polynom(polyint(intval(pp.c)));
        norm_sq = norm_sq + (primitive{xs(i+1)} - primitive{xs(i)});
    end
else
    norm_sq = 0;
    for i=1:length(xs)-1
        dx = xs(i+1) - xs(i);
        dv = v(i+1) - v(i);
        dvdx = dv/dx;

        p = [dvdx v(i)-dvdx*xs(i)];
        pp = conv(p,p);
        
        primitive = polyint(pp);
        norm_sq = norm_sq + diff(polyval(primitive, [xs(i) xs(i+1)]));
    end
end

norm = sqrt(norm_sq);

end

