function [M] = computeLN_sparse(xs,lambda,u,a,b,w,dflag,verbose)
%COMPUTELN Lemma 3.6.4
%   xs: Finite Element discretization
%   lambda: approximate eigenvalue
%   u: approximate eigenfunction (normalized)
%   a: SDE generator
%   b: SDE coeffs
%   w: normalization factor (assumed constant)

N = length(xs)-1;

total_iter = N-1+N-2+N-2+N-1+N-1;
% total_iter = N^2;
iter_count = 0;

i = zeros(1,total_iter);
j = zeros(1,total_iter);
v = zeros(1,total_iter);

if dflag
    a_primitive = polyint(a);
    b_x = polyder(b);
else
    a_primitive = polynom(polyint(intval(a)));
    w = intval(w);
    b = polynom(b);
    b_x = b';
    xs = intval(xs);
    u = intval(u);
    lambda = intval(lambda);
    v = intval(v);
end

u_x = (u(2:end)-u(1:end-1))./(xs(2:end)-xs(1:end-1));

idx = 1;

for ell=1:(N-1)
    for k=max(ell-1,1):min(ell+1,N-1)
        iter_count = iter_count + 1;
        if verbose
            print_status(iter_count, total_iter, 10, 'computeLN_sparse1');
        end
        
        i(idx) = ell;
        j(idx) = k;
        v(idx) = ...
                - h01(xs,a_primitive,k,ell) ...
                + bxl2(xs,b_x,k,ell) ...
                + bl2(xs,b,k,ell) ...
                - lambda*l2(xs,k,ell) ...
                ;
        idx = idx+1;
    end
end

for k=1:(N-1)
    iter_count = iter_count + 1;
    if verbose
        print_status(iter_count, total_iter, 10, 'computeLN_sparse2');
    end
        
    i(idx) = N;
    j(idx) = k;
    v(idx) = (xs(k+2)-xs(k))/2*w;
    idx = idx + 1;
end

for ell=1:(N-1)
    iter_count = iter_count + 1;
    if verbose
        print_status(iter_count, total_iter, 10, 'computeLN_sparse3');
    end
    
    i(idx) = ell;
    j(idx) = N;
    v(idx) = - uhl2(xs,u,u_x,ell);
    idx = idx + 1;
end
assert(sum(i == 0) == 0);
M = sparse(i,j,v,N,N);
end

function out = h01(xs,a_primitive,k,ell)
    if k == ell        
        out = (xs(k+1)-xs(k))^(-2) * (diffpolyval(a_primitive, [xs(k) xs(k+1)])) ...
            + (xs(k+2)-xs(k+1))^(-2) * (diffpolyval(a_primitive, [xs(k+1) xs(k+2)]));
    elseif abs(k-ell) == 1
        out = -1/(xs(k+2)-xs(k+1))^2 * diffpolyval(a_primitive, [xs(k+1) xs(k+2)]);
    else
        out = 0;
    end
end

function out = bxl2(xs,b_x,k,ell)
    if k == ell
        if isa(b_x, 'polynom')
            integrand1 = b_x*(polynom([1 -xs(k)]./(xs(k+1)-xs(k)))^2);
            primitive1 = polynom(polyint(intval(integrand1.c)));

            integrand2 = b_x*(polynom([-1 xs(k+2)]./(xs(k+2)-xs(k+1)))^2);
            primitive2 = polynom(polyint(intval(integrand2.c)));
        else
            p = [1 -xs(k)]./(xs(k+1)-xs(k));
            integrand1 = conv(b_x, conv(p,p));
            primitive1 = polyint(integrand1);

            p = [-1 xs(k+2)]./(xs(k+2)-xs(k+1));
            integrand2 = conv(b_x, conv(p,p));
            primitive2 = polyint(integrand2);
        end
        
        out = diffpolyval(primitive1, xs(k:(k+1))) ...
            + diffpolyval(primitive2, xs((k+1):(k+2)));
    elseif abs(k-ell) == 1
        p1 = [1 -xs(k+2)];
        p2 = [-1 xs(k+2)]./((xs(k+2)-xs(k+1))^2);
        if isa(b_x, 'polynom')
            integrand = b_x*polynom(p1)*polynom(p2);
            primitive = polynom(polyint(intval(integrand.c)));
        else
            integrand = conv(b_x, conv(p1, p2));
            primitive = polyint(integrand);
        end
        
        out = diffpolyval(primitive, xs((k+1):(k+2)));
    else
        out = 0;
    end
end

function out = bl2(xs,b,k,ell)
    if k == ell
        if isa(b, 'polynom')
            integrand1 = b*polynom([1 -xs(k)]./(xs(k+1)-xs(k))^2);
            primitive1 = polynom(polyint(intval(integrand1.c)));

            integrand2 = b*(-1/(xs(k+2)-xs(k+1))^2)*polynom([-1 xs(k+2)]);
            primitive2 = polynom(polyint(intval(integrand2.c)));
        else
            integrand1 = conv(b, [1 -xs(k)]./(xs(k+1)-xs(k))^2);
            primitive1 = polyint(integrand1);

            integrand2 = conv(b, (-1/(xs(k+2)-xs(k+1))^2).*[-1 xs(k+2)]);
            primitive2 = polyint(integrand2);
        end
        
        out = diffpolyval(primitive1, xs(k:k+1)) ...
            + diffpolyval(primitive2, xs(k+1:k+2));
    elseif abs(k - ell) == 1 % off diag
        if isa(b, 'polynom')
            integrand = b*polynom([-1 xs(k+2)]);
            primitive = polynom(polyint(intval(integrand.c)));
        else
            integrand = conv(b, [-1 xs(k+2)]);
            primitive = polyint(integrand);
        end
        
        out = diffpolyval(primitive, xs(k+1:k+2));
        out = out/(xs(k+2)-xs(k+1))^2;
        out = sign(k-ell)*out;
    else
        out = 0;
    end
end

function out = l2(xs,k,ell)
    if k == ell
        out = (xs(k+1)-xs(k))/3 + (xs(k+2)-xs(k+1))/3;
    elseif abs(k-ell) == 1
        out = (xs(k+2)-xs(k+1))/6;
    else
        out = 0;
    end
end

function out = uhl2(xs,u,u_x,ell)
    if isa(u, 'intval')
        integrand1 = ...
            polynom([u_x(ell) u(ell)-u_x(ell)*xs(ell)])...
            *polynom([1 -xs(ell)]./(xs(ell+1)-xs(ell)));
        primitive1 = polynom(polyint(intval(integrand1.c)));

        integrand2 = ...
            polynom([u_x(ell+1) u(ell+1)-u_x(ell+1)*xs(ell+1)])...
            *polynom([-1 xs(ell+2)]./(xs(ell+2)-xs(ell+1)));
        primitive2 = polynom(polyint(intval(integrand2.c)));
    else
        integrand1 = ...
            conv([u_x(ell) u(ell)-u_x(ell)*xs(ell)],...
            [1 -xs(ell)]./(xs(ell+1)-xs(ell)));
        primitive1 = polyint(integrand1);

        integrand2 = ...
            conv([u_x(ell+1) u(ell+1)-u_x(ell+1)*xs(ell+1)],...
            [-1 xs(ell+2)]./(xs(ell+2)-xs(ell+1)));
        primitive2 = polyint(integrand2);
    end

    out = diffpolyval(primitive1, xs(ell:ell+1)) ...
        + diffpolyval(primitive2, xs(ell+1:ell+2));
end

function out = diffpolyval(p, xs)
    if isa(p, 'polynom')
        tmp = p{xs};
    else
        tmp = polyval(p,xs);
    end
    out = tmp(2:end) - tmp(1:end-1);
end

