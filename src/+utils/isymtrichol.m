function [R] = isymtrichol(A,verbose)
%ISYMTRICHOL Interval Cholesky Decomposition
%   R = ichol(A) calculates the interval Cholesky factor such that A = R'*R
%   for interval matrix A that is symmetric tridiagonal and sparse
    [m,n] = size(A);

    diag_rows = 1:n;
    diag_cols = 1:n;
    diag = midrad(zeros(1,n),0);
    
    offdiag_rows = 2:n;
    offdiag_cols = 1:(n-1);
    offdiag = midrad(zeros(1,n-1),0);
    total_iter = n;
    iter_count = 0;
    for j=1:n
        iter_count = iter_count + 1;
        if verbose
            print_status(iter_count, total_iter, 10, 'isymtrichol');
        end
%         L(j,j) = (A(j,j) - sum(L(j,1:(j-1)).^2)).^(1/2);
        if j==1
            diag(j) = A(j,j).^(1/2);
        else
            diag(j) = (A(j,j) - offdiag(j-1).^2).^(1/2);
        end
%         if L(j,j).inf < 0
        if diag(j).inf < 0
            error('0 encountered in denominator for L(%d,%d)',j,j)
        end
        if j == n
            break
        end
        for i=(j+1)
%             L(i,j) = (A(i,j) - sum(L(i,1:(j-1)).*L(j,1:(j-1))))./L(j,j);
            offdiag(j) = A(i,j)./diag(j);
        end
    end
    L = sparse([diag_rows offdiag_rows],[diag_cols offdiag_cols],[diag offdiag]);
    R = L';
end

