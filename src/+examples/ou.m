function [u,lambda,KN,K,rho_1,rho_2,L1,rho,delta_u,err] = ou(alpha,sigma,xs,validate,knstrategy,verbose)
% inputs are:
%   alpha,sigma: coefficient values for the SDE dX = -alpha*x*dt + sigma*dW
%   xs: initial discretization of domain
%   validate: 0 = no, 1 = floating point, 2 = rigorous
%   knstrategy: 'neumann' = use neumann result, 'direct' = norm(M,2), 'best' = do both and take narrowest interval
%   verbose: 0 = silent, 1 = print status and other information

    G = [sigma];
    V = [-alpha 0];
    
    a = 1/2*conv(G,G);
    b = polysum(polyder(a), -V);
    
    a_p = polynom(a);
    assert(all(inf(a_p{intval(xs)}) > 0));

    [u,lambda] = fem.oned.solve(V,G,1,xs);
    
    u = [0;u(:,1);0];
    u = u';
    Iu = trapz(xs,u);
    w = 1/Iu;
    u = u.*w; 
    
    if validate ~= 0
        % kndflag = 1 if validate == 1
        % kndflag = 0 if validate == 2
        kndflag = mod(validate,2);
        [KN,K,rho_1,rho_2,L1,rho,delta_u,err] = verify(xs,lambda,u,w,a,b,kndflag,knstrategy,verbose);

        table(lambda, rho_1.sup, K.sup, rho.sup, L1.sup, delta_u, 'VariableNames',{'lambda', 'tau', 'K','rho','L1','delta_u'})
    else
        KN = -1;
        K = -1;
        rho_1 = -1;
        rho_2 = -1;
        L1 = -1;
        rho = -1;
        delta_u = -1;
    end
end