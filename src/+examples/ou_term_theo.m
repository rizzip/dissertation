n_paths = 50000;
n_steps = 10000;
dt = 1/n_steps;

paths = zeros(n_paths,n_steps);
finals = zeros(1,n_paths);
initials = zeros(1,n_paths);

distplot_xs = linspace(-5,5,1000);
pdfv = sqrt(1/pi)*exp(-distplot_xs.^2);

Iu = trapz(pdfv);

cdfv = cumtrapz(pdfv/Iu);
cdfv(cdfv<0) = 0;
cdfv(cdfv>1) = 1;
cdfv(end) = 1;

mu_0 = makedist('PiecewiseLinear','x',distplot_xs,'Fx',cdfv);

sample = @() icdf(mu_0,rand);

alpha = 1;
sigma = 1;

for i=1:n_paths
    print_status(i, n_paths, 10, 'mc')
%     x = randn; %0;
%     x = rand-0.5;
    x = sample(); %icdf(mu_0,rand);
    initials(i) = x;
    for j=2:n_steps
        dW = sqrt(dt)*randn;
        x = x + -alpha*x*dt + sigma*dW;
        paths(i,j) = x;
    end
    finals(i) = x;
end

figure
hold on
for i=1:200
    plot(linspace(0,1,n_steps), paths(i,:))
end

figure
hold on
histogram(finals,'Normalization','pdf')
plot(distplot_xs, pdf(mu_0,distplot_xs),'LineWidth',3)