function [u,lambda,KN,K,rho_1,rho_2,L1,rho,delta_u,err] = mine(c1,c2,s1,s2,xs,validate,knstrategy,verbose)
% inputs are:
%   c1,c2,s1,s2: coefficient values for the SDE dX = (c1*x-c2*x^3)dt + (s1*x^2+s2)dW
%   xs: initial discretization of domain
%   validate: 0 = no, 1 = floating point, 2 = rigorous
%   knstrategy: 'neumann' = use neumann result, 'direct' = norm(M,2), 'best' = do both and take narrowest interval
%   verbose: 0 = silent, 1 = print status and other information

    assert(sign(s1) == sign(s2), 's1, s2 must have the same sign')

    G = [s1 0 s2];
    V = [-c2 0 c1 0];
    
    a = 1/2*conv(G,G);
    b = polysum(polyder(a), -V);

    [u,lambda] = fem.oned.solve(V,G,1,xs);
    
    u = [0;u(:,1);0];
    u = u';
    Iu = trapz(xs,u);
    w = 1/Iu;
    u = u.*w;    
    
    if validate ~= 0
        assert(min(polyval(a, xs)) > 0);
        % kndflag = 1 if validate == 1
        % kndflag = 0 if validate == 2
        kndflag = mod(validate,2);
        [KN,K,rho_1,rho_2,L1,rho,delta_u,err] = verify(xs,lambda,u,w,a,b,kndflag,knstrategy,verbose);

        if err == 0
            table(lambda, rho_1.sup, K.sup, rho.sup, L1.sup, delta_u, 'VariableNames',{'lambda', 'tau', 'K','rho','L1','delta_u'})
            fprintf('%0.4e & %0.4f & %0.4f & %0.4e & $%0.2f\\pi^{-2}\\sqrt{2}$ & %0.4e \\\\ \\hline\n',lambda,rho_1.sup,K.sup,rho.sup,(xs(end)-xs(1))^2/4,delta_u)
        end
    else
        KN = -1;
        K = -1;
        rho_1 = -1;
        rho_2 = -1;
        L1 = -1;
        rho = -1;
        delta_u = -1;
        err = 0;
    end
end