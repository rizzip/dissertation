function f = polysum( p, q )
%POLYSUM Calculates the sum of two polynomials p and q
%   f = p + q
    Lp = length(p);
    Lq = length(q);
    L = max(Lp,Lq);
    if Lp == L
        f = p + [zeros(1,L - Lq) q];
    else
        f = [zeros(1,L - Lp) p] + q;
    end
end

