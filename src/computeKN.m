function [KN] = computeKN(xs,lambda,u,a,b,w,dflag,strategy,verbose)
%COMPUTEKN Lemma 3.6.5
%   xs: Finite Element discretization
%   lambda: approximate eigenvalue
%   u: approximate eigenfunction
%   a: SDE generator
%   b: SDE coeffs
%   w: normalization factor (assumed constant)
%   dflag: 1 = use FPA, 0 = use interval arithmetic
%   verbose: 1 = print progress, 0 = silent

Ctilde = computeCtilde(xs, a, dflag, verbose);
if dflag
    R = chol(Ctilde);
else
    R = utils.isymtrichol(Ctilde,verbose);
end
M = computeLN_sparse(xs,lambda,u,a,b,w,dflag,verbose);

if strcmp(strategy, 'neumann')
    [KN,~] = neumann(R,M,dflag);
elseif strcmp(strategy, 'direct')
    KN = norm(full(R*inv(full(M))*R'), 2);
elseif strcmp(strategy, 'best')
    [Kn_neumann,~,~,err] = neumann(R,M,dflag);
    Kn_direct = norm(full(R*inv(full(M))*R'), 2);
    
    if verbose
        Kn_neumann
        Kn_direct
    end

    if err
        warning on;
        warning('neumann error, using direct')
        KN = Kn_direct;
    else
        % both methods provide rigorous bounds
        % so we take the smallest width possible
        % ex: neumann = [3.1,3.4], direct = [0,8] --> [3.1,3.4]
        if dflag
            KN = min(Kn_neumann, Kn_direct);
        else
            KN = infsup(max(Kn_neumann,Kn_direct).inf, min(Kn_neumann,Kn_direct).sup);
        end
    end
end

end

function [KN,rho1,rho2,err] = neumann(R,M,dflag)
    if dflag
        B = inv(R')*M*inv(R);
        candidate = R*inv(M)*R';
    else
        B = inv(full(R'))*M*inv(full(R));
        candidate = R.mid*inv(M.mid)*R.mid';
    end
    residual = eye(size(candidate)) - candidate*B;

    rho1 = norm(residual, 2);
    rho2 = norm(full(candidate), 2);

    if rho1 < 1
        KN = rho2 / (1-rho1);
        err = 0;
    else
        warning on;
        if isa(rho1, 'intval')
            warning('rho1 ([%0.6f, %0.6f]) is too big. Unable to validate', inf(rho1), sup(rho1)) 
        else
            warning('rho1 (%0.6f) is too big. Unable to validate', rho1) 
        end
        KN = -1;
        err = 1;
    end
end
