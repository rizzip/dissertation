function print_status(iter_count, total_iter, interval, id)
    persistent next_print_pct;
    persistent first_tic;
    persistent last_print_tic;
    if isempty(next_print_pct) || iter_count == 1
        next_print_pct = 0;
    end
    if isempty(first_tic) || iter_count == 1
        first_tic = tic;
    end
    if isempty(last_print_tic) || iter_count == 1
        last_print_tic = first_tic;
    end
    if (iter_count/total_iter * 100) >= next_print_pct
        fprintf("%s: %05.2f %c, Interval: %.2f seconds, Cumulative: %.2f seconds, (%d of %d)\n",...
            id,...
            next_print_pct,...
            '%',...
            toc(last_print_tic),...
            toc(first_tic),...
            iter_count,...
            total_iter...
        );
        next_print_pct = next_print_pct + interval;
        last_print_tic = tic;
    end
end
