%as = flip(linspace(-3.4,0.4,50));
% c1s = flip(linspace(1e-3,0.8,4));
% c1s = [c1s flip(-c1s)];
% c1s = c1s(1:end-2);
c1s = [-0.5 -0.1 0.1 0.5 0.8];
xs = linspace(-3,3,4500);

% % nonuniform grid
% lxs = linspace(-10,-4,10);
% cxs = linspace(-4,4,10000);
% rxs = linspace(4,10,15);
% xs = [lxs cxs(2:end) rxs(2:end)];

plots = zeros(length(c1s),1);
gplots = zeros(length(c1s),1);
for i=1:length(c1s)
%     [P,D] = pitchfork_fem(c1s(i),1,0,1,1,xs);
    [P,D] = fem.oned.pitchfork(c1s(i),1,0,1,1,xs);
    P = [0;P(:,1);0];
    IP = trapz(xs,P);
    P = P./IP;
    figure(1);
    hold on
    plots(i) = plot(xs,P,'LineWidth',2,'DisplayName',sprintf("a = %0.3f",c1s(i)));
    % title(as(i))
    
    % Plot gradient of u as represented by P
    figure(2)
    hold on
    G = zeros(length(xs)-1,1);
    G([1 end]) = [P(1) -P(end)];
    for j=2:length(G)-1
        G(j) = (P(j)-P(j-1))/(xs(j)-xs(j-1));
    end
    gplots(i) = plot(xs(1:end-1),G,'LineWidth',2,'DisplayName',sprintf("c1 = %0.3f",c1s(i)));

    drawnow;
    pause(.2);
    % hold off
end
% Plot mesh (ONLY when N is small
% plot(xs(2:end-1),zeros(length(xs)-2),'ro')

legend(plots)
legend(gplots)
% legend(arrayfun(@(a) sprintf("%0.3f", a),as))

figure(1)
% set(gca, 'YLim', [0 0.5])
xlabel('x')